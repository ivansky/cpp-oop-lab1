#ifndef Vendor_hpp
#define Vendor_hpp

#include <string>
using namespace std;

enum Vendor {
    Apple,
    LG,
    Xiaomi,
    Meizu,
    Samsung,
    Microsoft,
    Google
};

inline const char* vendorName(const Vendor vendor) {
    switch(vendor) {
        case Apple: return "Apple";
        case LG: return "LG";
        case Xiaomi: return "Xiaomi";
        case Meizu: return "Meizu";
        case Samsung: return "Samsung";
        case Microsoft: return "Microsoft";
        case Google: return "Google";
        default: return "Unknown";
    }
}

#endif /* Vendor_hpp */
