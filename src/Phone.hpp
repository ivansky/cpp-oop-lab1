#ifndef Phone_hpp
#define Phone_hpp

#include <iostream>
#include <memory>
#include <stdio.h>
#include <string>
#include "Person.hpp"
#include "Vendor.hpp"

using namespace std;

class Phone {
private:
    string number;
    Vendor vendor;
    Person* owner;
public:
    Phone();
    Phone(const Vendor& vendor,
        const string& number);
    Phone(const Vendor& vendor,
        const string& number,
        Person* owner);
    void setVendor(const Vendor&);
    void setNumber(const string&);
    void setOwner(Person*);
    
    Vendor getVendor();
    string getNumber();
    Person* getOwner();
    string toString();
};

#endif /* Phone_hpp */
