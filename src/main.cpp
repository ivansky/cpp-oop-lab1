#include <iostream>
#include "Person.hpp"
#include "Phone.hpp"
#include "Vendor.hpp"

using namespace std;

int main(int argc, const char * argv[]) {
    
    auto ivan = new Person("Ivan", "Ivanov", "Ivanovich", 18);
    
    auto petr = new Person();
    petr->setFirstName("Petr");
    petr->setLastName("Petrov");
    petr->setMiddleName("Petrovich");
    petr->setAge(25);
    
    cout << ivan->toString() << endl;
    cout << petr->toString() << endl;
    
    auto phone = new Phone();
    
    phone->setNumber("+79120123456");
    phone->setOwner(petr);
    phone->setVendor(Apple);

    cout << phone->toString() << endl;

    return 0;
}
