#include "Person.hpp"
#include <stdio.h>

Person::Person() {
    setFirstName("");
    setLastName("");
    setMiddleName("");
    setAge(0);
}

Person::Person(const string& firstName,
               const string& lastName,
               const string& middleName,
               const int& age) {
    setFirstName(firstName);
    setLastName(lastName);
    setMiddleName(middleName);
    setAge(age);
}

void Person::setFirstName(const string& _firstName) {
    if(_firstName.size() > 20) {
        throw "Firstname should contain max 20 chars";
    }
    firstName = _firstName;
};

void Person::setLastName(const string& _lastName) {
    if(_lastName.size() > 20) {
        throw "Lastname should contain max 20 chars";
    }

    lastName = _lastName;
};

void Person::setMiddleName(const string& _middleName) {
    if(_middleName.size() > 20) {
        throw "Middlename should contain max 20 chars";
    }

    middleName = _middleName;
};

void Person::setAge(const int& _age) {
    if(_age < 0 || _age > 200) {
        throw "Can not set age less than 0 and bigger then 200";
    }
    age = _age;
};

string Person::getFirstName() {
    return firstName;
};

string Person::getLastName() {
    return lastName;
};

string Person::getMiddleName() {
    return middleName;
};

int Person::getAge() {
    return age;
};

string Person::toString() {
    char buffer[5];
    std::sprintf(buffer, "[%s %s %s, %d]",
                   getFirstName().c_str(),
                   getLastName().c_str(),
                   getMiddleName().c_str(),
                   getAge());
    return buffer;
}
