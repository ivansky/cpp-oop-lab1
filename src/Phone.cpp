#include "Phone.hpp"
#include "Vendor.hpp"
#include <stdio.h>

Phone::Phone() {
}

Phone::Phone(
    const Vendor& vendor,
    const string& number) {
        setVendor(vendor);
        setNumber(number);
}

Phone::Phone(
    const Vendor& vendor,
    const string& number,
    Person* owner) {
        setVendor(vendor);
        setNumber(number);
        setOwner(owner);
}

void Phone::setVendor(const Vendor& _vendor) {
    vendor = _vendor;
};

void Phone::setNumber(const string& _number) {
    if(_number.size() > 20) {
        throw "Number should contain max 20 chars";
    }
    number = _number;
};

void Phone::setOwner(Person* _owner) {
    owner = _owner;
};

Vendor Phone::getVendor() {
    return vendor;
}

string Phone::getNumber() {
    return number;
}

Person* Phone::getOwner() {
    return owner;
}

string Phone::toString() {
    char buffer[5];
    std::sprintf(buffer, "[%s %s (%s %s)]",
                   vendorName(getVendor()),
                   getNumber().c_str(),
                   getOwner()->getFirstName().c_str(),
                   getOwner()->getLastName().c_str());
    return buffer;
};
