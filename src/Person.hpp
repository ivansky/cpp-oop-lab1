#ifndef Person_hpp
#define Person_hpp

#include <iostream>
#include <stdio.h>
#include <string>

using namespace std;

class Person {
private:
    string firstName;
    string lastName;
    string middleName;
    int age;
public:
    Person();
    Person(const string& firstName,
           const string& lastName,
           const string& middleName,
           const int& age);
    void setFirstName(const string&);
    void setLastName(const string&);
    void setMiddleName(const string&);
    void setAge(const int&);
    
    string getFirstName();
    string getLastName();
    string getMiddleName();
    int getAge();
    
    string toString();
};

#endif /* Person_hpp */
