# Laboratory work №1.

## Necessary tools
- [**CMake**](https://cmake.org/download/)
- Compiler *(one of them)*
    * [**GCC 6 +**](https://gcc.gnu.org/install/binaries.html)
    * [**CLang 3.5 +**](http://releases.llvm.org/download.html)
    * [**MSVC 19 +**](http://landinghub.visualstudio.com/visual-cpp-build-tools)

## Build
Windows Cmd

`$ ./build.bat`

Unix / MacOS terminal

`$ chmod +x ./build.sh && ./build.sh`

## Entities
 * Person class
 * Vendor enum
 * Phone class 

## Person class:
Private fields:

- **First Name** - String ( 20 symbols )
- **Last Name** - String ( 20 symbols )
- **Middle Name** - String ( 20 symbols )
- **Age** - Integer

Public fields:

- Constructor
    * Default, without arguments, default values.
    * 4 arguments: first name, last name, middle name and age.
- Setters
    * Метод для установки возраста персоны, с проверкой на правильность ввода ( возраст >= 0 и <= 200 )
    * Метод для установки имени персоны, проверять длину строки.
    * Метод для установки фамилии персоны, проверять длину строки.
    * Метод для установки отчества персоны, проверять длину строки.
- Getters
    * Метод для получения возраста персоны.
    * Метод для получения имени персоны.
    * Метод для получения фамилии персоны.
    * Метод для получения отчества персоны.
- Other
    * Output method for printing summary information.

## Phone class
Private fields:

- владелец - поле типа Person
- марка - поле типа марка_телефона
- номер телефона - строка ( 12 символов )

Public fields:

- Конструктор устанавливающий свойства объекта по умолчанию.
- Конструктор с 2мя параметрами марка и номер телефона.
- Метод для установки владельца телефона, если возраст текущего владельца > возраста нового, выводить сообщение, и владельца не изменять.
- Метод для вывода информации о телефоне: марка, номер, фио владельца.

## Main() method
Main flow:

1. Создать 2 экземпляра класса  Person с разным значением возраста.
2. Вывести о них информацию на экран.
3. Создать экземпляр класса телефон.
4. Установить владельца телефона.
5. Вывести информацию об телефоне.
